const fetch = require('node-fetch');
const hashStringValue = require('./string');

const postToUrl = async (url, payload) => {
  if (url === undefined || url.length === 0) {
    throw Error('URL must not be undefined or empty');
  }
  try {
    const resp = await fetch(url, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(payload),
    });
    if (resp.status === 200) {
      console.log('in here');
      return resp.json();
    } else {
      const data = await resp.json();
      throw Error(data.error);
    }
  } catch (err) {
    throw Error('Network issue?');
  }
};

const success = 'http://localhost:1080/api/success';
const payload = '{"name" : "john smith"}';

postToUrl(success, payload)
    .then((data) => console.log(`Recieved: ${data.message}`))
    .catch((err) => console.error(`Error Raised: ${err}`));

const fail = 'http://localhost:1080/api/fail';

postToUrl(fail, payload)
    .then((data) => console.log(`Recieved: ${data.message}`))
    .catch((err) => console.error(`Error Raised: ${err}`));

const message = 'MESSAGE TO BE HASHED';

hashStringValue.hashStringHexValue(message)
    .then((hash) => console.log(`HEX =  ${hash}`))
    .catch((err) => console.error(err));

hashStringValue.hashStringBase64Value(message)
    .then((hash) => console.log(`Base64 =  ${hash}`))
    .catch((err) => console.error(err));
