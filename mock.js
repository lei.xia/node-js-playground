

const mockDelayWith = (decision) => {
  return new Promise((resolve, reject) => {
    if (decision) {
      setTimeout(() => resolve(`Everthing working fine`), 3000);
    } else {
      setTimeout(() => reject(Error(`Something went wrong`)), 5000);
    }
  });
};

module.exports = {mockDelayWith};
