const express = require('express');
const winston = require('winston');

const app = express();
const correlation = require('./middleware/correlation');
const requestLogger = require('./middleware/log-request');
const errorLogger = require('./middleware/log-error');
const path = require('path');

import nunjucks from 'nunjucks';

const consoleTransport = new winston.transports.Console();
const loggingOptions = {
  transports: [consoleTransport],
};

const logger = new winston.createLogger(loggingOptions);


const appViews = [
  path.join(__dirname, '/node_modules/govuk-frontend/'),
  path.join(__dirname, '/node_modules/govuk-frontend/components'),
  path.join(__dirname, '/views')
]

app.set('port', process.env.PORT || 3000);

// adds middleware
app.use(requestLogger(logger));
app.use(errorLogger(logger));

app.use(express.json());
app.use(correlation(logger));

nunjucks.configure(appViews, {
  autoescape: true,
  noCache: true,
  express: app
});

app.set('view engine', 'html');

app.get('/', (req, res) => {
  let data = {
    title: 'this is a test',
    layout: 'layout.njk',
    message: 'Hello world'
  };
  res.render('index.njk', data);
});

// setup api path
const apiRoute = require('./routes/claim');
app.use('/', apiRoute);
const healthCheckRoute = require('./routes/health');
app.use('/', healthCheckRoute);

// start server
app.listen(app.get('port'), () => {
  console.log(`server is listening ${app.get('port')}`);
});
