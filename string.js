const crypto = require('crypto');
const algorithm = 'sha256';

const hashStringHexValue = async (message) => {
  return crypto.createHash(algorithm)
      .update(message)
      .digest('hex');
};

const hashStringBase64Value = async (message) => {
  return crypto.createHash(algorithm)
      .update(message)
      .digest('base64');
};

module.exports = {hashStringHexValue, hashStringBase64Value};
