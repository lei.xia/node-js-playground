import controller from './claim-controller.js';

describe('claim controller handles claim request', () => {
  let res; let req;
  let Next;
  const claimRepository = require('../repository/claim-repository');
  describe('when get all claims request', () => {
    beforeEach(() => {
      res = {
        json: jest.fn(),
      };
      req = {};
      Next = jest.fn();
      claimRepository.findAllClaim = jest.fn().mockReturnValue([]);
    });
    it('should return 2 claims in JSON format', async () => {
      await controller.getAllClaims(req, res, Next);
      expect(res.json).toHaveBeenCalledWith([]);
    });
  });
  describe('when get claims by claim type request', () => {
    beforeEach(() => {
      res = {
        json: jest.fn(),
      };
      req = {
        params: {
          type: 'PIP',
        },
      };
      Next = jest.fn();
      claimRepository.findClaimByType =
        jest.fn()
            .mockReturnValue([{id: 123, type: 'PIP'}]);
    });
    it('should return only 1 claim in JSON format', async () => {
      await controller.getClaimByType(req, res, Next);
      expect(claimRepository.findClaimByType)
          .toHaveBeenCalledWith('PIP');
      expect(res.json)
          .toHaveBeenCalledWith([{id: 123, type: 'PIP'}]);
    });
  });
  describe('when post new claim with data', () => {
    beforeEach(() => {
      Next = jest.fn();
    });
    it('should return insertedId 456 in JSON format', async () => {
      claimRepository.postNewClaim =
      jest.fn()
          .mockReturnValue({insertedId: 456});
      res = {
        json: jest.fn(),
        status: jest.fn(),
      };
      req = {
        body: {'name': 'John Smith', 'age': 20},
      };
      await controller.addNewClaim(req, res, Next);
      expect(claimRepository.postNewClaim)
          .toHaveBeenCalledWith({name: 'John Smith', age: 20});
      expect(res.json).toHaveBeenCalledWith({insertedId: 456});
    });
    it('should return 400 status and message in JSON format', async () => {
      req = {
        body: {'name': '', 'age': 20},
      };
      res = {
        status: jest.fn(),
        json: jest.fn(),
      };
      await controller.addNewClaim(req, res, Next);
      expect(res.status)
          .toHaveBeenCalledWith(400);
      expect(res.json)
          .toHaveBeenCalledWith(
              {
                message: 'Error: Form validation failed on name empty input []',
              },
          );
    });
  });
});
