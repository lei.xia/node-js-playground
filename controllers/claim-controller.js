import claimRepository from '../repository/claim-repository.js'
import Form from '../models/form.js';

const getAllClaims = async (req, res, next) => {
  const result = await claimRepository.findAllClaim();
  res.json(result);
};

const getClaimByType = async (req, res, next) => {
  const result = await claimRepository.findClaimByType(req.params.type);
  res.json(result);
};

const addNewClaim = async (req, res, next) => {
  try {
    const data = new Form(req.body);
    const result = await claimRepository.postNewClaim(data);
    res.json(result);
  } catch (err) {
    res.status(400);
    res.json({message: err.toString()});
  }
};

module.exports = {getAllClaims, getClaimByType, addNewClaim};
