import { acquireCollection } from '../services/database';

const config = require('config');
const mongoConfig = config.get('claim-mgr.mongoConfig');
const { db, collection, url } = mongoConfig;

const findAllClaim = async () => {
  try {
    const claimRepo = await acquireCollection(url, db, collection);
    return await claimRepo.find({}).toArray();
  } catch (err) {
    console.error(err);
  }
};

const findClaimByType = async (val) => {
  try {
    const claimRepo = await acquireCollection(url, db, collection);
    const query = { type: val };
    return await claimRepo.find(query).toArray();
  } catch (err) {
    console.error(err);
  }
};

const postNewClaim = async (form) => {
  try {
    const claimRepo = await acquireCollection(url, db, collection);
    const result = await claimRepo.insertOne(
      {
        data: await encryption.encrypt(form.toString()),
        createdAt: new Date(),
      },
    );
    return new Promise((resolve, reject) => {
      if (result.insertedId) {
        resolve(result.insertedId);
      } else {
        reject(Error('insertion failed'));
      }
    });
  } catch (err) {
    console.error(err);
  }
};

module.exports = { findAllClaim, findClaimByType, postNewClaim };
