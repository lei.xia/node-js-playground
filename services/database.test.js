import MongoClient from 'mongodb';
import database from './database';
describe('mongo database operations', () => {
  const mongoUrl = 'mongodb://localhost:27017';
  const db = 'testDb';
  const collection = 'testColl';
  afterEach(() => {
    jest.restoreAllMocks();
  });
  describe('connect to given database on URL', () => {
    it('should connect to mongo', async () => {
      const connectSpy = jest.spyOn(MongoClient, 'connect')
          .mockReturnValueOnce({});
      const actual = await database.connect(mongoUrl);
      expect(actual).toEqual({});
      expect(connectSpy).toBeCalledWith(mongoUrl, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
      });
    });
  });
  describe('get database and collection', () => {
    it('should get given collection and database, connect should invoked',
        async () => {
          const client = {
            db: jest.fn().mockReturnThis(),
            collection: jest.fn(),
          };
          MongoClient.connect = jest.fn().mockReturnValue(client);
          await database.acquireCollection(mongoUrl,
              db,
              collection);
          expect(MongoClient.connect).toBeCalledWith(mongoUrl, {
            useNewUrlParser: true,
            useUnifiedTopology: true,
          });
          expect(client.db).toBeCalledWith(db);
          expect(client.collection).toBeCalledWith(collection);
        });
  });
});
