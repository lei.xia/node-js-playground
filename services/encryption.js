import AWS from 'aws-sdk';
import base64Fmt from 'base64';

const config = require('config');
const awsConfig = config.get('claim-mgr.awsConfig');

const kms = new AWS.KMS({
  accessKeyId: awsConfig.awsAccessKeyId,
  secretAccessKey: awsConfig.awsSecretAccessKey,
  region: awsConfig.region,
  endpoint: awsConfig.urlOverride === undefined ?
    null :
    awsConfig.urlOverride,
});

const encrypt = async (plain) => {
  const params = {
    KeyId: awsConfig.keyId,
    Plaintext: plain,
  };
  const {CiphertextBlob} = await kms.encrypt(params).promise();
  return CiphertextBlob.toString(base64Fmt);
};

const decrypt = async (cipher) => {
  const params = {
    CiphertextBlob: Buffer.from(cipher, base64Fmt),
  };
  const {Plaintext} = await kms.decrypt(params).promise();
  return Plaintext.toString();
};

module.exports = {encrypt, decrypt};
