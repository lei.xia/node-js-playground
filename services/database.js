import MongoClient from 'mongodb';

const connect = async (url) => {
  return MongoClient.connect(url,
      {
        useNewUrlParser: true,
        useUnifiedTopology: true,
      },
  );
};

const acquireCollection = async (url, db, coll) => {
  try {
    const client = await connect(url);
    return client.db(db).collection(coll);
  } catch (err) {
    console.error(err);
  }
};

module.exports = {connect, acquireCollection};
