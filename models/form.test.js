import Form from './form.js';

describe('form class object', () => {
  describe('when accept validated data', () => {
    it('should create a form object', () => {
      const data = {name: 'John Smith', age: 20};
      const actual = new Form(data);
      expect(actual.name).toBe('John Smith');
      expect(actual.age).toBe(20);
    });
  });
  describe('when accept invalid data', () => {
    it('should throw error on invalid age', () => {
      const data = {name: 'John Smith', age: 17};
      expect(() => {
        new Form(data);
      }).toThrow('Form validation failed on age input [17]');
    });
    it('should throw error on name', () => {
      const data = {name: '', age: 20};
      expect(() => {
        new Form(data);
      }).toThrow('Form validation failed on name empty input []');
    });
  });
  describe('serialize object to JSON string', () => {
    it('should print object in JSON string', () => {
      const data = {name: 'John Smith', age: 19};
      const form = new Form(data);
      expect(form.toJSONString()).toBe('{"name":"John Smith","age":19}');
    });
  });
});
