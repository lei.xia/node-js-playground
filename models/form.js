
class Form {
  constructor(data) {
    Object.assign(this, data);
    this.valid();
  }

  valid() {
    if (this.name.length === 0) {
      throw Error(`Form validation failed on name empty input [${this.name}]`);
    }
    if (this.age < 18) {
      throw new Error(`Form validation failed on age input [${this.age}]`);
    }
  }

  toJSONString() {
    return JSON.stringify(this);
  }
}

module.exports = Form;
