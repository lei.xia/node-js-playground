import express from 'express';

// eslint-disable-next-line
const router = express.Router();

router.get('/health', async (req, res, next) => {
  res.status(200).json({message: 'application up and running'});
});

module.exports = router;
