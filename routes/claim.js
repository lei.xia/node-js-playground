import express from 'express';
import claimController from '../controllers/claim-controller.js';

// eslint-disable-next-line
const router = express.Router();

router.get('/claims', claimController.getAllClaims);

router.get('/claim/:type', claimController.getClaimByType);

router.post('/claim', claimController.addNewClaim);

module.exports = router;
