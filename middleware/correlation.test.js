import correlation from './correlation.js';
import * as uuid from 'uuid';
jest.mock('uuid');

describe('correlation middleware', () => {
  describe('intercepts header with correlation id', () => {
    let logger; let Next;
    let req; let res;
    beforeEach(() => {
      logger = {
        info: jest.fn(),
      };
      Next = jest.fn();
    });
    it('should retrieve correlation from header and log it', () => {
      req = {
        headers: {'x-correlation-id': '123'},
      };
      const coll = correlation(logger);
      coll(req, res, Next);
      expect(logger.info).toHaveBeenCalledWith('123');
    });
    it('should generate random correlation id and log it', () => {
      const uuidSpy = jest.spyOn(uuid, 'v4').mockReturnValue('112358');
      req = {
        headers: {},
      };
      const coll = correlation(logger);
      coll(req, res, Next);
      expect(uuidSpy).toHaveBeenCalledTimes(1);
      expect(logger.info).toHaveBeenCalledWith('112358');
    });
  });
});
