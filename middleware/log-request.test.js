const fut = require('./log-request');

describe('logger request middleware', () => {
  describe('when middleware is invoked', () => {
    let logger; let Next;
    let req; let res;
    beforeEach(() => {
      logger = {
        info: jest.fn(),
        error: jest.fn(),
      };
      req = () => {
        return {
          url: '/claim',
        };
      };
      Next = jest.fn();
    });
    it('logger is called to log request', () => {
      const logRequest = fut(logger);
      logRequest(req, res, Next);
      expect(logger.info).toHaveBeenCalledWith(req.url);
    });
  });
});
