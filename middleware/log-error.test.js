const fut = require('./log-error');

describe('log error middleware', () => {
  describe('when middleware is invoked', () => {
    let logger; let Next;
    let req; let res; let error;
    beforeEach(() => {
      error = {
        message: 'something went wrong',
      };
      logger = {
        info: jest.fn(),
        error: jest.fn(),
      };
      req = () => {
        return {
          url: '/claim',
        };
      };
      Next = jest.fn();
    });
    it('logger is called to log error', () => {
      const logError = fut(logger);
      logError(error, req, res, Next);
      expect(logger.error).toHaveBeenCalledWith(error);
    });
  });
});
