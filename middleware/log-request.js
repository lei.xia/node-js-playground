module.exports = (logger) => {
  return (req, res, next) => {
    logger.info(req.url);
    next();
  };
};
