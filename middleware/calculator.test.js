import sum from './calculator.js';

describe('sum all numbers', () => {
  it('calculator add is invoked Spied On', () => {
    const add = require('./computer');
    const spiedOnAdd = jest.spyOn(add, 'add');
    expect(sum(2, 2)).toBe(4);
    expect(spiedOnAdd).toHaveBeenCalledWith(2, 2);
  });
  it('calculator add is invoked with mock', () => {
    const computer = require('./computer');
    computer.add = jest.fn().mockReturnValue(7);
    expect(sum(2, 5)).toBe(7);
    expect(computer.add).toHaveBeenCalledWith(2, 5);
  });
});
