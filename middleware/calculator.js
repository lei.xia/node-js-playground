import {add} from './computer.js';

const sum = (a, b) => {
  return add(a, b);
};

export default sum;
