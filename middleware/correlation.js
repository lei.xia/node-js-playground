const {v4: uuidv4} = require('uuid');

const randomUUID = () => {
  return uuidv4();
};

module.exports = (logger) => {
  return (req, res, next) => {
    if (req.headers['x-correlation-id'] !== undefined) {
      logger.info(req.headers['x-correlation-id']);
    } else {
      logger.info(randomUUID());
    }
    next();
  };
};
