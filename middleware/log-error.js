
module.exports = (logger) => {
  return (err, req, res, next) => {
    logger.error(err);
    next();
  };
};
