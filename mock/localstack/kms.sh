#!/bin/bash

KEY_ID=$(awslocal kms create-key --query 'KeyMetadata.KeyId' --output text)
awslocal kms create-alias --allias-name alias/test-key --target-key-id "$KEY_ID"
awslocal kms describe-key --key-id alias/test-key